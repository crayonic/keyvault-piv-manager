SET "PATH=%PATH%;C:\Users\Timotej Stanek\AppData\Local\Programs\Common\Microsoft\Visual C++ for Python\9.0\WinSDK\Bin"
SET "PATH=%PATH%;C:\Program Files (x86)\NSIS"
SET "PATH=%PATH%;C:\Python27-win32\Lib\site-packages\PySide"
SET "PIVTOOL_VERSION=1.4.6"

REM Download yubico-piv-tool DLLs
REM powershell -Command "(New-Object Net.WebClient).DownloadFile('https://developers.yubico.com/yubico-piv-tool/Releases/yubico-piv-tool-%PIVTOOL_VERSION%-win32.zip', 'yubico-piv-tool-%PIVTOOL_VERSION%-win32.zip')"
REM 7z e "-olib yubico-piv-tool-%PIVTOOL_VERSION%-win32.zip" bin/

rem Python2.7 32 bit, because yubico-piv-tool it 32 bit
rem python -m pip install PyInstaller==3.6 PySide==1.2.4
rem Disable SignTool
rem NSIS 2.06.1
rem if nsis cannot find some file from resources, copy it to temp directory it is looking for it
rem https://www.microsoft.com/en-us/download/details.aspx?id=44266

python setup.py qt_resources
python setup.py install
python setup.py executable
