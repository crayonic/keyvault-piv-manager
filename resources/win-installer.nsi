!include "MUI2.nsh"

; Installer icon
!define MUI_ICON "keyvault-piv-manager.ico"

; The name of the installer
Name "KeyVault PIV Manager"

; The file to write
OutFile "../dist/keyvault-piv-manager-${VERSION}-win.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\KeyVault PIV Manager"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Crayonic\KeyVault PIV Manager" "Install_Dir"

SetCompressor /SOLID lzma
ShowInstDetails show

;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------

; Pages
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

; Languages
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------

Section "KeyVault PIV Manager"
  ; Remove all
  DELETE "$INSTDIR\*"

  SectionIn RO
  SetOutPath $INSTDIR
  FILE "..\dist\KeyVault PIV Manager\*"
SectionEnd

Var MYTMP

# Last section is a hidden one.
Section
  WriteUninstaller "$INSTDIR\uninstall.exe"

  ; Write the installation path into the registry
  WriteRegStr HKLM "Software\Crayonic\KeyVault PIV Manager" "Install_Dir" "$INSTDIR"

  # Windows Add/Remove Programs support
  StrCpy $MYTMP "Software\Microsoft\Windows\CurrentVersion\Uninstall\KeyVault PIV Manager"
  WriteRegStr       HKLM $MYTMP "DisplayName"           "KeyVault PIV Manager"
  WriteRegExpandStr HKLM $MYTMP "DisplayIcon"           "$INSTDIR\pivman.exe"
  WriteRegExpandStr HKLM $MYTMP "UninstallString"       '"$INSTDIR\uninstall.exe"'
  WriteRegExpandStr HKLM $MYTMP "QuietUninstallString"  '"$INSTDIR\uninstall.exe" /S'
  WriteRegExpandStr HKLM $MYTMP "InstallLocation"       "$INSTDIR"
  WriteRegStr       HKLM $MYTMP "DisplayVersion"        "${VERSION}"
  WriteRegStr       HKLM $MYTMP "Publisher"             "Crayonic B.V."
  WriteRegStr       HKLM $MYTMP "URLInfoAbout"          "https://www.crayonic.com"
  WriteRegDWORD     HKLM $MYTMP "NoModify"              "1"
  WriteRegDWORD     HKLM $MYTMP "NoRepair"              "1"
  ; Hardcoded size of  37MB
  WriteRegDWORD     HKLM $MYTMP "EstimatedSize"         0x00009400

  ; Create shortcuts
  SetShellVarContext all
  CreateShortCut "$SMPROGRAMS\KeyVault PIV Manager.lnk" "$INSTDIR\pivman.exe" "" "$INSTDIR\pivman.exe" 0
  CreateShortCut "$SMSTARTUP\KeyVault PIV Manager PIN Expiration Check.lnk" "$INSTDIR\pivman.exe" "-c"
SectionEnd

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KeyVault PIV Manager"
  DeleteRegKey HKLM "Software\Crayonic\KeyVault PIV Manager"
  DeleteRegKey /ifempty HKCU "Software\Crayonic\KeyVault PIV Manager"

  ; Remove files
  DELETE "$INSTDIR\*"

  ; Remove shortcuts
  SetShellVarContext all
  Delete "$SMPROGRAMS\KeyVault PIV Manager.lnk"
  Delete "$SMSTARTUP\KeyVault PIV Manager PIN Expiration Check.lnk"

  ; Remove directories used
  RMDir "$INSTDIR"
SectionEnd
